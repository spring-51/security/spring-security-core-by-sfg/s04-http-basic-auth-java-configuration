# Http Basic Auth Java Configuration

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

### L24 - permit all with url pattern matching
```
1. When we add security dependency,by default http basic auth configured for all the apis.

2. If we want to whitelist some apis then we need to override default configurtion
  - we need to create a configurtion class which override defalt http basice spring config
    (refer com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig)
  - add @Configuration annotation to the SecurityConfig class
  - add @EnableWebSecurity annotation to the SecurityConfig class
    --- this will mark class as which is responsible for security
  - extend "WebSecurityConfigurerAdapter"
    -- this creates bean of WebSecurityConfigurerAdapter type and restrict by default security bean
    -- WebSecurityConfigurerAdapter - this class contains many overloaded methods
      --- for Authentication
        ---- configure(HttpSecurity) - is responsible for authentication and whiteliting APIs
        ---- by default it's securing all apis (refer WebSecurityConfigurerAdapter -> configure(HttpSecurity) )
      --- for Authorization
      --- whitelisting APIs
        ---- --- configure(HttpSecurity) - is responsible for authentication and whiteliting APIs

3. Important Class and method
- @EnableWebSecurity
- WebSecurityConfigurerAdapter
- antMatchers
  -- antMatcher are used to make som apis bypassing spring security.
    --- whenever we use antMatcher a filter is auto create by Spring in SpringSecurityFilterChain to by pass spring 
        security.
    --- we can use wild card here
      ---- eg1 using absolute url
         http.authorizeRequests((requests) -> {
            requests.antMatchers("/whitelist/public").permitAll();
        });
          ----- this will whitelist only one api i.e. {baseEndpoint}/whitelist/public
      
      ---- eg2 using  /* wildcard as suffix
         http.authorizeRequests((requests) -> {
            requests.antMatchers("/whitelist/*").permitAll();
        });
          ----- this will whitelist apis which has exact one word after "/whitelist" i.e. {baseEndpoint}/whitelist/public, {baseEndpoint}/whitelist/students
          ----- this will NOT whitelist apis which has more than one word after "/whitelist" i.e. {baseEndpoint}/whitelist/public/students
     
     ---- eg3 using  /** wildcard as suffix
         http.authorizeRequests((requests) -> {
            requests.antMatchers("/whitelist/**").permitAll();
        });
          ----- this will whitelist apis which has any number of word after "/whitelist" i.e. {baseEndpoint}/whitelist/public/students, {baseEndpoint}/whitelist/public etc
      
      ---- eg4 using */ wildcard as prefix
      
- mvcMatchers - its same as as antMatchers only differernece in syntax of path 
http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
            ;
        });
        
        - refer com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
```

### L25 - Http method matching
```
1.  Using Spring security we can whitlist path with specific Http Method
i.e. we want to whitelist "/whitelist/get" path for Http GET APIs, but for POST we want Spring security.

http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for ONLY Http GET
                    .antMatchers(HttpMethod.GET, "/whitelist/get").permitAll()
            ;
        });
        
        - refer com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
```

### L26 - Spring MVC Path matching
```
1. using ant matcher we can whitelist api.
2. Alternatively we can use mvcMatchers which take string sme as @RequestMapping

http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
            ;
        });
        
        - refer com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
```