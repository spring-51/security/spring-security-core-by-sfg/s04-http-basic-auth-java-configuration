package com.udemy.sfg.s04httpbasicauth.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/whitelist")
public class WhitelistController {

    @GetMapping("/public")
    public String publicAPI(){
        return "Hello from public API";
    }

    @GetMapping("/get")
    public String getAPI(){
        return "Hello from public API GET **/get";
    }

    @PostMapping("/get")
    public String postAPI(){
        return "Hello from public API POST **/get";
    }

    @GetMapping("/{path}")
    public String pathVariableAPI(@PathVariable String path){
        return "Hello from pathVariableAPI , Path varible is - "+path;
    }
}
