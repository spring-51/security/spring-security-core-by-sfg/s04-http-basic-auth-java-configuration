package com.udemy.sfg.s04httpbasicauth.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // requests.antMatchers(..) is used for whitelisting APIs
        // order of ant matcher is important
        // we can place requests.antMatchers(..) after requests.anyRequest()
        // it will give RTE as java.lang.IllegalStateException: Can't configure antMatchers after anyRequest
        http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for all Http methods
                    .antMatchers("/whitelist/public", "/").permitAll()

                    // this will whitelist api for ONLY Http GET
                    .antMatchers(HttpMethod.GET, "/whitelist/get").permitAll()

                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
            ;
        });
        http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });

        http.formLogin();
        http.httpBasic();
    }
}
