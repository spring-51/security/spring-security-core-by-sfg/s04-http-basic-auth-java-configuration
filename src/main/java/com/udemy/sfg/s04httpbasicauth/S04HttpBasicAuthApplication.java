package com.udemy.sfg.s04httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S04HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S04HttpBasicAuthApplication.class, args);
    }

}
