package com.udemy.sfg.s04httpbasicauth.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class StudentControllerTests extends BaseTests {


    // username and password in optional, but annotation is mandatory
    // @WithMockUser(username = "anything", password = "anything")
    // @WithMockUser(username = "anything")
    @WithMockUser
    @Test
    void getStudents() throws Exception{
        mockMvc.perform(get("/students"))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasic() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("admin", "admin")))
                .andExpect(status().isOk());
    }
}
